package com.itbaizhan.travel.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbaizhan.travel.mapper.CategoryMapper;
import com.itbaizhan.travel.pojo.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class CategoryService {
    @Autowired
    private CategoryMapper categoryMapper;

    /**
     * 分页查询
     * @param page
     * @param size
     * @return
     */
    public Page<Category> findPage(int page,int size){
        Page<Category> selectPage = categoryMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }

    /**
     * 新增
     * @param category
     */
    public void insert(Category category){
        categoryMapper.insert(category);
    }

    /**
     * 根据id查询产品类型
     * @param cid
     * @return
     */
    public Category selectById(Integer cid){
        return categoryMapper.selectById(cid);
    }

    /**
     * 修改
     * @param category
     */
    public void update(Category category){
        categoryMapper.updateById(category);
    }

    public void delete(Integer cid){
        categoryMapper.deleteById(cid);
    }

    public List<Category> findAll(){
        return categoryMapper.selectList(null);
    }
}
