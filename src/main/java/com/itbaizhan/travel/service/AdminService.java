package com.itbaizhan.travel.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbaizhan.travel.bean.RoleWithStatus;
import com.itbaizhan.travel.mapper.AdminMapper;
import com.itbaizhan.travel.mapper.RoleMapper;
import com.itbaizhan.travel.pojo.Admin;
import com.itbaizhan.travel.pojo.Permission;
import com.itbaizhan.travel.pojo.Role;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class AdminService {
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private BCryptPasswordEncoder encoder;
    /**
     * 分类查询管理员
     * @param page 页数
     * @param size 每页条数
     * @return
     */
    public Page<Admin> findPage(int page, int size){
        Page<Admin> selectPage = adminMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }
    public List<RoleWithStatus> findRole(Integer aid){
        //1.查询所有的角色
        List<Role> roles = roleMapper.selectList(null);
        //2.查询用户拥有的所有角色id
        List<Integer> rids = roleMapper.findRoleIdByAdmin(aid);
        List<RoleWithStatus> roleList = new ArrayList();
        for (Role role:roles){
            RoleWithStatus roleWithStatus = new RoleWithStatus();
            BeanUtils.copyProperties(role,roleWithStatus);
            if (rids.contains(role.getRid())){
                roleWithStatus.setAdminHas(true);
            }else {
                roleWithStatus.setAdminHas(false);
            }
            roleList.add(roleWithStatus);
        }
        return roleList;
    }

    /**
     * 新增管理员
     * @param admin
     */
    public void add(Admin admin){
        admin.setPassword(encoder.encode(admin.getPassword()));
        adminMapper.insert(admin);
    }

    /**
     * 根据id查询管理员信息
     * @param aid
     * @return
     */
    public Admin selectById(Integer aid){
        Admin admin = adminMapper.selectById(aid);
        return admin;
    }
    /**
     * 修改管理员
     * @param admin
     */
    public void update(Admin admin){
        String oldPassword = adminMapper.selectById(admin.getAid()).getPassword();
        String newpassword = admin.getPassword();
        if (oldPassword != newpassword){
            admin.setPassword(encoder.encode(admin.getPassword()));
        }
        adminMapper.updateById(admin);
    }

    /**
     * 查询用户详情
     * @param aid
     * @return
     */
    public Admin findDesc(Integer aid){
        return adminMapper.findDesc(aid);
    }

    public void updateRole(Integer aid, Integer[] ids) {
        // 删除用户的所有角色
        adminMapper.deleteAdminAllRoles(aid);
        // 重新给用户添加角色
        for (Integer rid : ids) {
            adminMapper.addAdminRole(aid, rid);
        }
    }

    /**
     * 根据id修改状态
     * @param aid
     */
    public void updateStatus(Integer aid) {
        Admin admin = adminMapper.selectById(aid);
        admin.setStatus(!admin.isStatus());
        adminMapper.updateById(admin);
    }

    /**
     * 根据名字查询管理员信息
     * @param username
     * @return
     */
    public Admin findByAdminName(String username){
        QueryWrapper<Admin> wrapper = new QueryWrapper();
        wrapper.eq("username",username);
        Admin admin = adminMapper.selectOne(wrapper);
        return admin;
    }

    public List<Permission> findAllPermission(String username){
        return adminMapper.findAllPermission(username);
    }
}
