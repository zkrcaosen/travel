package com.itbaizhan.travel.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.itbaizhan.travel.bean.Result;
import com.itbaizhan.travel.mapper.MemberMapper;
import com.itbaizhan.travel.pojo.Member;
import com.itbaizhan.travel.util.MailUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class MemberService {
    @Autowired
    private MemberMapper memberMapper;
    @Autowired
    private BCryptPasswordEncoder encoder;

    @Value("${project.Path}")
    private String projectPath;

    @Autowired
    private MailUtils mailUtils;
    public Result register(Member member){
        //1.保存用户
        //判断用户名是否重复
        QueryWrapper<Member> wrapper = new QueryWrapper();
        wrapper.eq("username", member.getUsername());
        List<Member> members = memberMapper.selectList(wrapper);
        if (members.size()>0){
            return new Result(false,"用户名重复");
        }

        //判断邮箱是否重复
        QueryWrapper<Member> wrapper1 = new QueryWrapper();
        wrapper1.eq("email", member.getEmail());
        List<Member> members1 = memberMapper.selectList(wrapper1);
        if (members1.size()<0){
            return new Result(false,"邮箱重复");
        }
        //判断手机号是否重复
        QueryWrapper<Member> wrapper2 = new QueryWrapper();
        wrapper2.eq("phoneNum", member.getPhoneNum());
        List<Member> members2 = memberMapper.selectList(wrapper2);
        if (members2.size()<0){
            return new Result(false,"手机号已存在");
        }
        //加密
        String password = member.getPassword();
        password = encoder.encode(password);
        member.setPassword(password);
        //设置用户状态为false
        member.setActive(false);
        //2.激活
        //生成激活码
        String activeCode = UUID.randomUUID().toString();
        // 给用户的邮箱发送一封邮件，该邮件包含一个链接，链接中包含激活码
        String activeUrl =projectPath+"/frontdesk/member/active?activeCode="+activeCode;
        String text = "恭喜您注册成功！<a href = '"+activeUrl+"'>点击激活</a>完成账号认证";
        mailUtils.sendMail(member.getEmail(),text,"旅游网激活邮件");
        member.setActiveCode(activeCode);
        memberMapper.insert(member);
        return new Result(true,"注册成功");
    }

    //激活用户
    public String active(String activeCode){
        //根据激活码查询用户
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("activeCode",activeCode);
        Member member = memberMapper.selectOne(queryWrapper);
        if (member == null){
            return "激活失败！激活码错误!";
        }else {
            member.setActive(true);
            memberMapper.updateById(member);
        }
        return "激活成功，请<a href='"+projectPath+"/frontdesk/login'>登 录</a>";
    }
    public Result login(String name,String password){
        Member member = null;

        if (member == null){
            QueryWrapper<Member> queryWrapper = new QueryWrapper();
            queryWrapper.eq("username",name);
            member = memberMapper.selectOne(queryWrapper);
        }
        if (member == null){
            QueryWrapper<Member> queryWrapper = new QueryWrapper();
            queryWrapper.eq("email",name);
            member = memberMapper.selectOne(queryWrapper);
        }
        if (member == null){
            QueryWrapper<Member> queryWrapper = new QueryWrapper();
            queryWrapper.eq("phoneNum",name);
            member = memberMapper.selectOne(queryWrapper);
        }
        if (member == null){
            return new Result(false,"用户名或密码错误");
        }
        //验证密码
        boolean flag = encoder.matches(password, member.getPassword());
        if (!flag){
            return new Result(false,"用户名或密码错误");
        }
        if (!member.isActive()){
            return new Result(false,"用户未激活，请登录邮箱激活用户！");
        }
        return new Result(true,"登陆成功",member);
    }

}
