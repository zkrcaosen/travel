package com.itbaizhan.travel.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbaizhan.travel.mapper.PermissionMapper;
import com.itbaizhan.travel.pojo.Permission;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PermissionService {
    @Autowired
    private PermissionMapper permissionMapper ;

    /**
     * 查询权限信息
     * @param page 页数
     * @param size 每页条数
     * @return
     */
    public Page<Permission> findPage(int page, int size){
        Page<Permission> selectPage = permissionMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }

    /**
     * 新增权限
     * @param permission
     */
    public void insert(Permission permission){
        permissionMapper.insert(permission);
    }

    /**
     * 根据id查询权限信息
     * @param pid
     * @return
     */
    public Permission findPermission(Integer pid){
        return permissionMapper.selectById(pid);
    }

    /**
     * 修改权限信息
     * @param permission
     */
    public void update(Permission permission){
        permissionMapper.updateById(permission);
    }

    public void delete(Integer pid){
        permissionMapper.deleteById(pid);
    }
}
