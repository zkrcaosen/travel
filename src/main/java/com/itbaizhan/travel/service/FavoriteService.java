package com.itbaizhan.travel.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbaizhan.travel.mapper.ProductMapper;
import com.itbaizhan.travel.pojo.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FavoriteService {
    @Autowired
    private ProductMapper productMapper;

    public boolean findFavorite(Integer pid,Integer mid){
        int favoritePidAndMid = productMapper.findFavoritePidAndMid(pid, mid);
        if (favoritePidAndMid == 0){ //没有收藏
            return false;
        }else{
            return true;
        }

    }

    /**
     * 收藏
     * @param pid
     * @param mid
     */
    public void addFavorite(Integer pid,Integer mid){
        productMapper.addFavorite(pid, mid);
    }
    /**
     * 取消收藏
     * @param pid
     * @param mid
     */
    public void delFavorite(Integer pid,Integer mid){
        productMapper.delFavorite(pid, mid);
    }

    public Page<Product> findMemberFavorite(Page<Product> page,Integer mid){
        return productMapper.findMemberFavorite(page, mid);
    }
}
