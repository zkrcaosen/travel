package com.itbaizhan.travel.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbaizhan.travel.bean.PermissionWithStatus;
import com.itbaizhan.travel.mapper.PermissionMapper;
import com.itbaizhan.travel.mapper.RoleMapper;
import com.itbaizhan.travel.pojo.Permission;
import com.itbaizhan.travel.pojo.Role;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RoleService {
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private PermissionMapper permissionMapper;
    /**
     * 分类查询角色
     * @param page 页数
     * @param size 每页条数
     * @return
     */
    public Page<Role> findPage(int page, int size){
        Page<Role> selectPage = roleMapper.selectPage(new Page(page, size), null);
        return selectPage;
    }

    /**
     * 新增角色
     * @param role
     */
    public void insert(Role role){
        roleMapper.insert(role);
    }
    /**
     * 根据id查询角色信息
     * @param rid
     * @return
     */
    public Role selectById(Integer rid){
        return roleMapper.selectById(rid);
    }

    /**
     * 修改角色
     * @param role
     */
    public void update(Role role){
        roleMapper.updateById(role);
    }

    /**
     * 删除角色
     * @param rid
     */
    public void delete(Integer rid){
        roleMapper.deleteById(rid);
    }

    public List<PermissionWithStatus> findPermission(Integer rid){
        //1. 查询所有权限
        List<Permission> permissions = permissionMapper.selectList(null);
        //2. 查询角色拥有的所有权限id
        List<Integer> pids = permissionMapper.findPermissionIdByRole(rid);
        //3. 新建带有状态的权限集合
        List<PermissionWithStatus> permissionList = new ArrayList();
        for (Permission permission : permissions) {
            PermissionWithStatus permissionWithStatus= new PermissionWithStatus();
            BeanUtils.copyProperties(permission,permissionWithStatus);
            if (pids.contains(permission.getPid())){
                permissionWithStatus.setRoleHas(true);
            }else {
                permissionWithStatus.setRoleHas(false);
            }
            permissionList.add(permissionWithStatus);
        }
        return permissionList;
    }

    /**
     *修改权限
     * @param rid
     * @param ids
     */
    public void updatePermission(Integer rid,Integer [] ids){
        roleMapper.deleteRoleAllPermission(rid);
        for (Integer pid: ids){
            roleMapper.addRolePermission(rid,pid);
        }
    }

}
