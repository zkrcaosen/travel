package com.itbaizhan.travel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itbaizhan.travel.pojo.Role;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RoleMapper extends BaseMapper<Role> {
    /**
     * 查询用户拥有的所有角色的id
     * @param aid 角色id
     * @return
     */
    List<Integer> findRoleIdByAdmin(Integer aid);

    /**
     * 根据id删除所有角色
     * @param rid
     */
    void deleteRoleAllPermission(Integer rid);

    /**
     * 添加角色
     * @param rid
     * @param pid
     */
    void addRolePermission(@Param("rid")Integer rid , @Param("pid") Integer pid);
}
