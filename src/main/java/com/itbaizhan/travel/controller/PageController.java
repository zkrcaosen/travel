package com.itbaizhan.travel.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PageController {
    /**
     * 后台页面
     * @param page
     * @return
     */
    @RequestMapping("/backstage/{page}")
    public String showPageBackstage(@PathVariable String page){
        return "/backstage/"+page;
    }

    /**
     * 前台页面
     * @param page
     * @return
     */
    @RequestMapping("/frontdesk/{page}")
    public String showPageFrontDesk(@PathVariable String page){
        return "/frontdesk/"+page;
    }

    @RequestMapping("favicon.ico")
    @ResponseBody
    void ReturnNoFavicon(){

    }
}
