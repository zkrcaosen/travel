package com.itbaizhan.travel.controller.frontdesk;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itbaizhan.travel.pojo.Member;
import com.itbaizhan.travel.pojo.Product;
import com.itbaizhan.travel.service.FavoriteService;
import com.itbaizhan.travel.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/frontdesk/product")
public class FrontdeskProductController {
    @Autowired
    private ProductService productService;
    @Autowired
    private FavoriteService favoriteService;
    @RequestMapping("/routeList")
    public ModelAndView findProduct(Integer cid, String productName,
                                    @RequestParam(defaultValue = "1") int page,
                                    @RequestParam(defaultValue = "10")int size){
        ModelAndView modelAndView = new ModelAndView();
        Page<Product> productPage = productService.findProduct(cid, productName, page, size);
        modelAndView.addObject("productPage",productPage);
        modelAndView.addObject("cid",cid);
        modelAndView.addObject("productName",productName);
        modelAndView.setViewName("/frontdesk/route_list");
        return modelAndView;

    }

    @RequestMapping("/routeDetail")
    public ModelAndView findOne(Integer pid, HttpSession session){
        Object member = session.getAttribute("member");
        ModelAndView modelAndView = new ModelAndView();
        if (member == null){//未登录未收藏
            modelAndView.addObject("favorite",false);
        }else {
            Member member1 = (Member) member;
            boolean favorite = favoriteService.findFavorite(pid, member1.getMid());
            modelAndView.addObject("favorite",favorite);
        }
        Product product = productService.findOne(pid);
        modelAndView.addObject("product",product);
        modelAndView.setViewName("/frontdesk/route_detail");
        return modelAndView;
    }
}
