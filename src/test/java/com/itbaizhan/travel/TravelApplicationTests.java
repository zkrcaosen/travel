package com.itbaizhan.travel;

import com.itbaizhan.travel.mapper.AdminMapper;
import com.itbaizhan.travel.service.AdminService;
import com.itbaizhan.travel.util.MailUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class TravelApplicationTests {
    @Autowired
    private AdminService adminService;
    @Autowired
    private AdminMapper adminMapper;
    @Autowired
    private MailUtils mailUtils;

    @Test
    void contextLoads() {
//        Page<Admin> adminPage = adminService.findPage(1, 5);
//        System.out.println(adminPage);
//        Admin desc = adminMapper.findDesc(1);
//        System.out.println(desc);
//        List<RoleWithStatus> role = adminService.findRole(1);
//        System.out.println(role);
        mailUtils.sendMail("1312655990@qq.com","这是一封测试邮件","测试");
    }

}
